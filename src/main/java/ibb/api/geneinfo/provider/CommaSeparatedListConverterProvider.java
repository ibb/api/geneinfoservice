package ibb.api.geneinfo.provider;

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

import javax.ws.rs.ext.ParamConverter;
import javax.ws.rs.ext.ParamConverterProvider;
import javax.ws.rs.ext.Provider;

@Provider
public class CommaSeparatedListConverterProvider implements ParamConverterProvider {

	@SuppressWarnings("unchecked")
	@Override
	public <T> ParamConverter<T> getConverter(Class<T> rawType, Type genericType, Annotation[] annotations) {
		for (Annotation annotation : annotations) {
			if (CommaSeparatedList.class.isInstance(annotation)) {
				return (ParamConverter<T>) new CommaSeparatedListConverter();
			}
		}
		return null;
	}

}
