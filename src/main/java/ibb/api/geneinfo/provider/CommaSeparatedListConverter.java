package ibb.api.geneinfo.provider;

import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;

import java.util.List;
import java.util.stream.Stream;

import javax.ws.rs.ext.ParamConverter;

public class CommaSeparatedListConverter implements ParamConverter<List<String>>{

	@Override
	public List<String> fromString(String value) {
		return value == null ? List.of() : Stream.of(value.split(",")).collect(toList());
	}

	@Override
	public String toString(List<String> value) {
		return value == null ? null : value.stream().collect(joining(","));
	}

}
