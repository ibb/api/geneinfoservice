package ibb.api.geneinfo.resource;

import static java.util.stream.Collectors.toList;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Stream;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.parameters.Parameter;
import org.eclipse.microprofile.openapi.annotations.parameters.Parameters;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponses;

import ibb.api.geneinfo.model.TriboliumGene;
import ibb.api.geneinfo.parser.Util;

@Deprecated
@Path("/tribolium/genes")
@Produces(MediaType.APPLICATION_JSON)
public class TriboliumGeneResource {

    @GET
    @Path("/{id: TC[0-9]{6}}")
    @Operation(summary = "Get information for a tribolium gene.")
    @Parameters({
        @Parameter(name = "id", description = "Tribolium gene identifier in format TC[0-9]{6}}", example = "TC016177")
    })
    @APIResponses({
        @APIResponse(responseCode = "200", description = "Success"),
        @APIResponse(responseCode = "404", description = "Not found")
    })
    public TriboliumGene getOne(@PathParam("id") String id) {
        return (TriboliumGene) TriboliumGene.findByIdOptional(id).orElseThrow(NotFoundException::new);
    }
    
    
    @GET
    @Operation(summary = "Get information of a list of tribolium genes")
    @Parameters({
        @Parameter(name = "ids", description = "Comma-separated list of Tribolium gene identifiers in format TC[0-9]{6}}", example = "TC016177,TC001906")
    })
    @APIResponses({
        @APIResponse(responseCode = "200", description = "Success")
    })
    public List<TriboliumGene> getAll(@DefaultValue("") @QueryParam("ids") String ids) {
        
        Set<String> idSet = Util.trimAndSplit(ids);
        Stream<TriboliumGene> genes;
        
        if (!idSet.isEmpty()) {
            genes = idSet.stream()
                    .map(id -> TriboliumGene.findByIdOptional(id))
                    .filter(Optional::isPresent)
                    .map(o -> (TriboliumGene) o.get());
       
        } else {
            genes = TriboliumGene.streamAll();
        }
        return genes.collect(toList());
    }
}
