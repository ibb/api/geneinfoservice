package ibb.api.geneinfo.resource;

/*
@Path("/")
public class GeneinfoResource {
	// private static final Logger LOG = Logger.getLogger(GeneinfoResource.class);
	
	@GET
	@Path("/species")
	@Operation(summary = "Get a list of available species")
	public List<Species> listSpecies() {
		return Species.listAll();
	}
	
	@GET
	@Path("/species/{name}")
	@Operation(summary = "Get detail information of a species")
	public Species getSpecies(@PathParam("name") String name) {
		return (Species) Species.findByIdOptional(name).orElseThrow(NotFoundException::new);
	}
	
	@GET
    @Path("/species/{name}/genes")
    @Operation(summary = "Get information of a list of genes in a species")
    @Parameters({
        @Parameter(name = "ids",
        		description = "Comma-separated list of gene identifiers", 
        		schema = @Schema(implementation = String.class),
				example = "FBgn0000015,FBgn0012142")})
	public List<Gene> listSpeciesGenes(
			@PathParam("name") String speciesName,
			@QueryParam("ids") @CommaSeparatedList List<String> ids,
			@QueryParam("symbol") Optional<String> symbolOptional,
			@QueryParam("name") Optional<String> nameOptional,
			@QueryParam("annoId") Optional<String> annoIdOptional,
			@QueryParam("page") @DefaultValue("0") int pageIndex,
			@QueryParam("size") @DefaultValue("20") int pageSize) {
		return listGenes(ids, symbolOptional, nameOptional, annoIdOptional, Optional.of(speciesName), pageIndex, pageSize);
	}
	
	@GET
	@Path("/species/{name}/genes/{id}")
    @Operation(summary = "Get detail information of one gene in a species")
    @Parameters({
    	@Parameter(name = "name", description = "Species name", example = "dmel"),
        @Parameter(name = "id", description = "Gene identifier", example = "FBgn0000015")})
	public Gene getSpeciesGene(
			@PathParam("name") String name,
			@PathParam("id") String id) {
		return (Gene) Gene.findByIdOptional(id)
				.filter(gene -> Objects.equals(((Gene) gene).species.name, name))
				.orElseThrow(NotFoundException::new);
	}
	
	@GET
    @Path("/genes")
    @Operation(summary = "Get information of a list of genes")
    @Parameters({
        @Parameter(name = "ids",
        		description = "Comma-separated list of gene identifiers", 
        		schema = @Schema(implementation = String.class),
				example = "FBgn0000015,FBgn0012142")})
	public List<Gene> listGenes(
			@QueryParam("ids") @CommaSeparatedList List<String> ids,
			@QueryParam("symbol") Optional<String> symbolOptional,
			@QueryParam("name") Optional<String> nameOptional,
			@QueryParam("annoId") Optional<String> annoIdOptional,
			@QueryParam("speciesName") Optional<String> speciesNameOptional,
			@QueryParam("page") @DefaultValue("0") int pageIndex,
			@QueryParam("size") @DefaultValue("20") int pageSize) {
		
		List<String> queries = new ArrayList<>();
		Map<String, Object> params = new HashMap<>();
		
		if (!ids.isEmpty()) {
			queries.add("id IN :ids");
			params.put("ids", ids);
		}
		nameOptional.ifPresent(name -> {
			queries.add("name = :name");
			params.put("name", name);
		});
		symbolOptional.ifPresent(symbol -> {
			queries.add("symbol = :symbol");
			params.put("symbol", symbol);
		});
		annoIdOptional.ifPresent(annoId -> {
			queries.add("annoId = :annoId");
			params.put("annoId", annoId);
		});
		speciesNameOptional.ifPresent(speciesName -> {
			queries.add("speciesName = :speciesName");
			params.put("speciesName", speciesName);
		});
		
		PanacheQuery<Gene> genes = queries.isEmpty() ? Gene.findAll()
				: Gene.find(queries.stream().collect(joining(" AND ")), params);
		return genes.page(Page.of(pageIndex, pageSize)).list();
	}
	
	@GET
	@Path("/genes/{id}")
    @Operation(summary = "Get detail information of one gene")
    @Parameters({
        @Parameter(name = "id", description = "Gene identifier", example = "FBgn0000015")})
	public Gene getGene(@PathParam("id") String id) {
		return (Gene) Gene.findByIdOptional(id).orElseThrow(NotFoundException::new);
	}
}
*/