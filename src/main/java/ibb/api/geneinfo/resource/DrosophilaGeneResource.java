package ibb.api.geneinfo.resource;

import static java.util.stream.Collectors.toList;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Stream;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.parameters.Parameter;
import org.eclipse.microprofile.openapi.annotations.parameters.Parameters;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponses;

import ibb.api.geneinfo.model.DrosophilaGene;
import ibb.api.geneinfo.parser.Util;

@Deprecated
@Path("/drosophila/genes")
@Produces(MediaType.APPLICATION_JSON)
public class DrosophilaGeneResource {

    @GET
    @Path("/{id: FBgn[0-9]{7}}")
    @Operation(summary = "Get information of a drosophila gene")
    @Parameters({
        @Parameter(name = "id", description = "Flybase gene identifier in format FBgn[0-9]{7}", example = "FBgn0000015")
    })
    @APIResponses({
        @APIResponse(responseCode = "200", description = "Success"),
        @APIResponse(responseCode = "404", description = "Not found")
    })
    public DrosophilaGene getOne(@PathParam("id") String id) {
        return (DrosophilaGene) DrosophilaGene.findByIdOptional(id).orElseThrow(NotFoundException::new);
    }
    
    
    @GET
    @Path("/")
    @Operation(summary = "Get information of a list of drosophila genes")
    @Parameters({
        @Parameter(name = "ids", description = "Comma-separated list of Drosophila gene identifiers in format FBgn[0-9]{7}", example = "FBgn0000015,FBgn0000008"),
        @Parameter(name = "symbol"),
        @Parameter(name = "fullname"),
        @Parameter(name = "annotationId")
    })
    @APIResponses({
        @APIResponse(responseCode = "200", description = "Success"),
        @APIResponse(responseCode = "400", description = "Bad request. More than one query parameter were supplied")
    })
    public List<DrosophilaGene> getAll(
    		@DefaultValue("") @QueryParam("ids") String ids,
            @QueryParam("symbol") String symbol,
            @QueryParam("fullname") String fullname,
            @QueryParam("annotationId") String annotationId) {
        
        Set<String> idSet = Util.trimAndSplit(ids);
        
        long paramCnt = Stream.of(symbol, fullname, annotationId).filter(Objects::nonNull).count();
        if (paramCnt > 0 && idSet.size() > 0 || idSet.size() == 0 && paramCnt > 1) {
            throw new BadRequestException("Only one query param is allowed");
        }
        
        Stream<DrosophilaGene> genes;
        
        if (!idSet.isEmpty()) {
            genes = idSet.stream()
                    .map(id -> DrosophilaGene.findByIdOptional(id))
                    .filter(Optional::isPresent)
                    .map(o -> (DrosophilaGene) o.get());
        } else if (symbol != null) {
            genes = DrosophilaGene.findBySymbol(symbol).stream();
        } else if (fullname != null) {
            genes = DrosophilaGene.findByFullname(fullname).stream();
        } else if (annotationId != null) {
            genes = DrosophilaGene.findByAnnotationId(annotationId).stream();
        } else {
            genes = DrosophilaGene.streamAll();
        }
        return genes.collect(toList());
    }
}
