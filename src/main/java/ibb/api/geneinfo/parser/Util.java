package ibb.api.geneinfo.parser;

import static java.util.stream.Collectors.toCollection;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Predicate;
import java.util.function.Supplier;

public class Util {
    public static <C extends Collection<String>> C trimAndSplit(String str, Supplier<C> collectionFactory) {
        return Arrays.stream(str.trim().split(","))
                .map(String::trim)
                .filter(Predicate.not(""::equals))
                .collect(toCollection(collectionFactory));
    }
    
    public static <C extends Collection<String>> Set<String> trimAndSplit(String str) {
    	return trimAndSplit(str, HashSet::new);
    }
}
