package ibb.api.geneinfo.parser;

import static java.util.stream.Collectors.toMap;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;

import org.jboss.logging.Logger;

public class TabularRecord {
    private static final Logger LOGGER = Logger.getLogger(TabularRecord.class);
    
    private List<String> headers;
    private List<String> values;
    private Map<String, String> valueMap;
    
    public TabularRecord(List<String> headers, String line, String delimiter) {
        this.headers = headers;
        this.values = Arrays.asList(line.split(delimiter));
        
        try {
            this.valueMap = IntStream.range(0, values.size())
                    .boxed()
                    .collect(toMap(headers::get, values::get));
        } catch (ArrayIndexOutOfBoundsException e) {
            LOGGER.errorv("Too many values\n\n"
                    + "Headers ({0}):\n\t{1}\n\n"
                    + "Values ({2}):\n\t{3}\n",
                    headers.size(), String.join("\n\t", headers), values.size(), String.join("\n\t", values));
            throw new RuntimeException();
        }
    }
    
    public String get(String key) {
        return valueMap.get(key);
    }
    
    public String get(int index) {
        return values.get(index);
    }
    
    public List<String> getHeaders() {
        return headers;
    }
}
