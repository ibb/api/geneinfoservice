package ibb.api.geneinfo.parser;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class GFFRecord {
    private String seqname;
    private String source;
    private String feature;
    private String start;
    private String end;
    private String score;
    private String strand;
    private String frame;
    private Map<String, String> attributes;
    
    public GFFRecord(String line) {
        
        List<String> cols = Arrays.stream(line.split("\t"))
                .map(col -> ".".equals(col) ? null : col)
                .collect(toList());
        
        seqname = cols.get(0);
        source = cols.get(1);
        feature = cols.get(2);
        start = cols.get(3);
        end = cols.get(4);
        score = cols.get(5);
        strand = cols.get(6);
        frame = cols.get(7);
        
        if (cols.get(8) != null) {
            attributes = Arrays.stream(cols.get(8).split(";"))
                    .map(pair -> pair.split("="))
                    .filter(pair -> pair.length == 2)
                    .collect(toMap(pair -> pair[0], pair -> pair[1]));
        }
    }

    public String getSeqname() {
        return seqname;
    }

    public String getSource() {
        return source;
    }

    public String getFeature() {
        return feature;
    }

    public String getStart() {
        return start;
    }

    public String getEnd() {
        return end;
    }

    public String getScore() {
        return score;
    }

    public String getStrand() {
        return strand;
    }

    public String getFrame() {
        return frame;
    }

    public Map<String, String> getAttributes() {
        return attributes;
    }
}
