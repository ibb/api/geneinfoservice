package ibb.api.geneinfo.parser;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.zip.GZIPInputStream;

import org.jboss.logging.Logger;

public class Parser {
    private static final Logger LOGGER = Logger.getLogger(Parser.class);
    
    public static void parseGFF(String filepath, Consumer<GFFRecord> consumer) {
        LOGGER.infov("Parsing {0}", filepath);
        Optional.of(filepath)
            .map(Parser::getURL)
            .ifPresent(url -> {
                try {
                    InputStream inputStream = new GZIPInputStream(url.openStream());
                    BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
                    reader.lines()
                        .filter(line -> !line.startsWith("#"))
                        .map(GFFRecord::new)
                        .forEach(consumer::accept);
                } catch (IOException e) {
                    LOGGER.errorv("Can not open {0} for reading", filepath);
                }
            });
        LOGGER.infov("Parsed {0}", filepath);
    }
    
    public static void parseFasta(String filepath, Consumer<FastaRecord> consumer) {
        LOGGER.infov("Parsing {0}", filepath);
        Optional.of(filepath)
            .map(Parser::getURL)
            .ifPresent(url -> {
                try {
                    InputStream inputStream = new GZIPInputStream(url.openStream());
                    BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
                    
                    StringBuilder header = new StringBuilder();
                    StringBuilder sequence = new StringBuilder();
                    
                    reader.lines()
                        .filter(line -> !line.startsWith(";"))
                        .forEach(line -> {
                            if (line.startsWith(">")) {
                                if (sequence.length() > 0) {
                                    consumer.accept(new FastaRecord(header.toString(), sequence.toString()));
                                }
                                // Start new fasta record
                                header.setLength(0);
                                header.append(line.substring(1));
                                sequence.setLength(0);
                            } else {
                                sequence.append(line);
                            }
                        });
                    if (sequence.length() > 0) {
                        consumer.accept(new FastaRecord(header.toString(), sequence.toString()));
                    }
                    
                } catch (IOException e) {
                    LOGGER.errorv("Can not open {0} for reading", filepath);
                }
            });
        LOGGER.infov("Parsed {0}", filepath);
    }
    
    public static void parseTSV(String filepath, List<String> headers, Consumer<TabularRecord> consumer) {
        LOGGER.infov("Parsing {0}", filepath);
        Optional.of(filepath)
            .map(Parser::getURL)
            .ifPresent(url -> {
                try {
                    InputStream inputStream = new GZIPInputStream(url.openStream());
                    BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
                    reader.lines()
                        .filter(line -> !line.startsWith("#"))
                        .map(line -> new TabularRecord(headers, line, "\t"))
                        .forEach(consumer::accept);
                } catch (IOException e) {
                    LOGGER.errorv("Can not open {0} for reading", filepath);
                }
            });
        LOGGER.infov("Parsed {0}", filepath);
    }

    private static URL getURL(String str) {
        try {
            if (str.startsWith("/")) {
                str = "file://" + str;
            }
            URI uri = new URI(str);
            if (!uri.isAbsolute()) {
                uri = Paths.get(str).toAbsolutePath().toUri();
            }
            return uri.toURL();
        } catch (URISyntaxException | IllegalArgumentException | MalformedURLException e) {
            LOGGER.errorv("Bad URL {0}", str);
        }
        return null;
    }
}