package ibb.api.geneinfo.parser;

public class FastaRecord {
    private String header;
    private String sequence;
    
    public FastaRecord(String header, String sequence) {
        this.header = header;
        this.sequence = sequence;
    }

    public String getHeader() {
        return header;
    }

    public String getSequence() {
        return sequence;
    }
}
