package ibb.api.geneinfo;

import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.core.Application;

import org.eclipse.microprofile.openapi.annotations.OpenAPIDefinition;
import org.eclipse.microprofile.openapi.annotations.info.Info;

@OpenAPIDefinition(
        info = @Info(title = "Gene Information Service",
                description = "Get general information for Drosophila and Tribolium genes.",
                version = "0.1")
)
@ApplicationScoped
public class GeneinfoService extends Application {
}