package ibb.api.geneinfo.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import io.quarkus.runtime.annotations.RegisterForReflection;

@Entity
@RegisterForReflection
public class Species extends PanacheEntityBase {

	@Id
	public String name;
	
	@OneToMany(mappedBy = "species")
	public List<Gene> genes;
}
