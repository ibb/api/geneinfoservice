package ibb.api.geneinfo.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import io.quarkus.runtime.annotations.RegisterForReflection;

@RegisterForReflection
@Deprecated
@Entity
public class DrosophilaGene extends PanacheEntityBase {
	
	@Id
    public String id;
    public String symbol;
    public String fullname;
    public String annotationId;
    
    public static List<DrosophilaGene> findByFullname(String fullname) {
    	return find("fullname", fullname).list();
    }
    
    public static List<DrosophilaGene> findBySymbol(String symbol) {
    	return find("symbol", symbol).list();
    }
    
    public static List<DrosophilaGene> findByAnnotationId(String annotationId) {
    	return find("annotationId", annotationId).list();
    }
}
