package ibb.api.geneinfo.model;

import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import io.quarkus.runtime.annotations.RegisterForReflection;

@Entity
@RegisterForReflection
public class Gene extends PanacheEntityBase {

	@Id
	public String id;
	public String symbol;
	public String name;
	public String annoId;
	
	@ManyToOne
	@JoinColumn(name = "species", referencedColumnName = "name")
	public Species species;
	
	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass())
			return false;
		Gene gene = (Gene) o;
		return Objects.equals(id, gene.id);
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(id);
	}
}
