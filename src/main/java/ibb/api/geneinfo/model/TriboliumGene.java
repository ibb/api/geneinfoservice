package ibb.api.geneinfo.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import io.quarkus.runtime.annotations.RegisterForReflection;

@RegisterForReflection
@Deprecated
@Entity
public class TriboliumGene extends PanacheEntityBase {

	@Id
    public String id;
	public String seqname;
	public String start;
	public String end;
	public String strand;

	@Column(columnDefinition = "LONGTEXT")
	public String CDS;
	
	@Column(columnDefinition = "LONGTEXT")
	public String mRNA;
	
	@Column(columnDefinition = "LONGTEXT")
	public String protein;
}