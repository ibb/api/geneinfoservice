package ibb.api.geneinfo.loader;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.Dependent;
import javax.transaction.Transactional;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.jboss.logging.Logger;

import ibb.api.geneinfo.model.DrosophilaGene;
import ibb.api.geneinfo.parser.Parser;
import io.quarkus.runtime.Startup;

@Deprecated
@Startup
@Dependent
public class DrosophilaGeneLoader {
	private static final Logger LOG = Logger.getLogger(DrosophilaGeneLoader.class);
	
    @ConfigProperty(name = "data.drosophila.gene.tsv")
    String geneSetPath;
	
	@PostConstruct
	@Transactional
    public void load() {
		if (DrosophilaGene.count() == 0) {
		
			LOG.info("Attempting to initialize Drosophila data...");
	        
	        Parser.parseTSV(geneSetPath, List.of(
	                "organism",
	                "gene_type",
	                "gene_ID",
	                "gene_symbol",
	                "gene_fullname",
	                "annotation_ID",
	                "transcript_type",
	                "transcript_ID",
	                "transcript_symbol",
	                "polypeptide_ID",
	                "polypeptide_symbol"), record -> {
	            
	            if (!"Dmel".equals(record.get("organism"))) return;
	            
	            String id = record.get("gene_ID");
	            
	            DrosophilaGene gene = DrosophilaGene.findById(id);
	            if (gene != null) return;
	            
	            gene = new DrosophilaGene();
	            gene.id = id;
	            gene.symbol = record.get("gene_symbol");
	            gene.fullname = record.get("gene_fullname");
	            gene.annotationId = record.get("annotation_ID");
	            gene.persist();
	        });
		}
        
        LOG.infov("Drosophila gene count: {0}", DrosophilaGene.count());
	}
}
