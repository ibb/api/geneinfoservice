package ibb.api.geneinfo.loader;

import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;
import javax.enterprise.context.Dependent;
import javax.transaction.Transactional;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.jboss.logging.Logger;

import ibb.api.geneinfo.model.TriboliumGene;
import ibb.api.geneinfo.parser.FastaRecord;
import ibb.api.geneinfo.parser.GFFRecord;
import ibb.api.geneinfo.parser.Parser;
import io.quarkus.runtime.Startup;

@Deprecated
@Startup
@Dependent
public class TriboliumGeneLoader {
	private static final Logger LOG = Logger.getLogger(TriboliumGeneLoader.class);
	private static final Pattern TC_PATTERN = Pattern.compile("(TC[0-9]{6})");
    
    @ConfigProperty(name = "data.tribolium.gene.gff")
    String gff;
    
    @ConfigProperty(name = "data.tribolium.cds.fasta")
    String cdsFasta;
    
    @ConfigProperty(name = "data.tribolium.mrna.fasta")
    String mRNAFasta;
    
    @ConfigProperty(name = "data.tribolium.protein.fasta")
    String proteinFasta;
    
    @PostConstruct
    @Transactional
    public void load() {
    	if (TriboliumGene.count() == 0) {
	    	LOG.info("Attempting to initialize Tribolium data...");
	    	loadGenes();
	    	loadCDS();
	    	loadMRNAs();
	    	loadProteins();
    	}
        LOG.infov("Tribolium gene count: {0}", TriboliumGene.count());
    }
    
    @Transactional
    private void loadGenes() {
    	Parser.parseGFF(gff, record -> Optional.of(record)
            .filter(r -> "gene".equals(r.getFeature()))
            .map(this::getTCNo)
            .map(id -> {
            	TriboliumGene gene = new TriboliumGene();
            	gene.id = id;
            	gene.seqname = record.getSeqname();
            	gene.start = record.getStart();
            	gene.end = record.getEnd();
            	gene.strand = record.getStrand();
            	return gene;
            })
            .ifPresent(gene -> gene.persist()));
    }
    
    @Transactional
    private void loadCDS() {
    	Parser.parseFasta(cdsFasta, record -> Optional.of(record)
            .map(FastaRecord::getHeader)
            .map(this::getTCNo)
            .map(id -> TriboliumGene.findById(id))
            .ifPresent(obj -> {
            	TriboliumGene gene = (TriboliumGene) obj;
            	gene.CDS = record.getSequence();
            	gene.persist();
            }));
    }
    
    @Transactional
    private void loadMRNAs() {
        Parser.parseFasta(mRNAFasta, record -> Optional.of(record)
            .map(FastaRecord::getHeader)
            .map(this::getTCNo)
            .map(id -> TriboliumGene.findById(id))
            .ifPresent(obj -> {
            	TriboliumGene gene = (TriboliumGene) obj;
            	gene.mRNA = record.getSequence();
            	gene.persist();
            }));
    }
    
    @Transactional
    private void loadProteins() {
        Parser.parseFasta(proteinFasta, record -> Optional.of(record)
            .map(FastaRecord::getHeader)
            .map(this::getTCNo)
            .map(id -> TriboliumGene.findById(id))
            .ifPresent(obj -> {
            	TriboliumGene gene = (TriboliumGene) obj;
            	gene.protein = record.getSequence();
            	gene.persist();
            }));
    }
    
    private String getTCNo(GFFRecord record) {
        return getTCNo(record.getAttributes().get("locus_tag"));
    }
    
    private String getTCNo(String str) {
        Matcher matcher = TC_PATTERN.matcher(str);
        return matcher.find() ? matcher.group(1) : null;
    }
}
