# Gene Info Service for the iBeetle-Base project

## Development

To run in development mode (hot deployment with background compilation)

```
./mvnw quarkus:dev
```

To create a jar file

```
./mvnw clean package
```

To create a native app inside a container

```
./mvnw clean package -Pnative -Dquarkus.container-image.build=true
```
